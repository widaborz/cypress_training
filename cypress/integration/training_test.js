describe('Training test', () => {
    it('make an assertion', () =>{
        let my_var = true; 
        expect(my_var).to.equal(true);
    })

    it('navigate webjive integration test and try to login and logout', () =>{
        cy.visit('https://integration.engageska-portugal.pt/testdb/devices');
        cy.contains('Log In').click();
        cy.get('[type="text"]').type('CREAM').should('have.value', 'CREAM')
        cy.get('[type="password"]').type('CREAM_SKA').should('have.value', 'CREAM_SKA')
        cy.get('.modal-footer [type="submit"]').click()
        cy.get('.LogInOut span').contains('CREAM')
        cy.contains('Log Out').click();        
    })
})