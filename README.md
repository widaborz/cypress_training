# Cypress training

To install the necessary environment 

``` bash
npm install cypress
```

To run the environment, in the folder: 

``` bash
./node_modules/.bin/cypress open
```

There are two tests in place: 

- a simple assertion
- a test that navigates https://integration.engageska-portugal.pt/testdb and performs a login/logout action using CREAM credentials.  
